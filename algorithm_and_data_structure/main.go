package main

import "fmt"

func main() {

	theALgo := func(n int) int {
		m := 0
		for i := 0; i < n; i++ {
			m += 1
		}
		return m
	}(100)

	//fmt.Println(theALgo)
	fmt.Println("N = 100, Number of instructions o(n) ::", theALgo)
}
