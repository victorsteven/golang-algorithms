package main

import (
	"fmt"
	"strings"
)

func main(){
	var input string
	_, _ = fmt.Scanf("%s\n", &input)
	answer := 0
	//b := []byte(input)
	//fmt.Println(b)
	for _, ch := range input {
		str := string(ch)
		if strings.ToUpper(str) == str {
			answer++
		}
		//min, max := 'A', 'Z'
		//if ch >= min && ch <= max {
		//	//It is a capital letter!
		//	answer++
		//}
		//fmt.Printf("%T %T\n", min, ch)
	}
	fmt.Println(answer)
	//fmt.Println("Input is: ", input)
}
