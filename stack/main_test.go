package main

import (
	"fmt"
	"testing"
)

var myStruct = Stack{}
func TestPeek(t *testing.T) {
	//myStruct.mySlice = []int{2,3,4,5,6,7,8}
	myStruct.mySlice = []int{}

	ans := myStruct.peek()
	fmt.Println("this is the output: ", ans)
}

func TestPop(t *testing.T) {
	myStruct.mySlice = []int{2,3,4,5,6,7,8}

	ans := myStruct.pop()
	fmt.Println("this is the output: ", ans)

	fmt.Println("this is the resulting array: ", myStruct.mySlice)
}

func TestPush(t *testing.T) {
	myStruct.mySlice = []int{2,3,4,5,6,7,8}

	myStruct.push(30)

	fmt.Println("this is the resulting array: ", myStruct.mySlice)
}