package main

import "fmt"

type Stack struct {
	mySlice []int
}

//Print the stacks like an array
func (s *Stack) String() string {
	return fmt.Sprint(s.mySlice)
}

func main() {
	var s = new(Stack)
	s.push(200)
	s.push(300)
	s.push(400)
	s.push(500)
	s.push(600)
	s.push(700)
	s.push(800)
	s.peek()
	s.pop()
	s.pop()
	fmt.Println(s)
}

//optional, we can use it to check the length of the stack
//func (s *Stack) size() int {
//	return len(s.mySlice)
//}

func (s *Stack) push(i int) {
	s.mySlice = append(s.mySlice, i)
}

func (s *Stack) peek() int {
	if len(s.mySlice) > 0 {
		return s.mySlice[len(s.mySlice)-1]
	}
	return -1
}

func (s *Stack) pop() int {
	var ret = s.peek() //get the first item in the slice
	if len(s.mySlice) > 0 {
		s.mySlice = s.mySlice[0 : len(s.mySlice)-1]
	} else {
		return -1
	}
	return ret
}