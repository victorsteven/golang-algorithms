package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSquareArray(t *testing.T) {

	arraySamples := []struct {
		arr []int
		ans []int
	}{
		{
			arr: []int{1,2,3,4, 5},
			ans: []int{1,4,9,16, 25},
		},
		{
			arr: []int{},
			ans: nil,
		},
	}
	for _, v := range arraySamples {
		result := arraySquares(v.arr)
		assert.EqualValues(t, v.ans, result)
	}
}