package main

import "fmt"

func SquareDigit(number int) []int {
	var arr = []int{}
	for number != 0 {
		digit := number % 10
		arr = append(arr, digit)

		number /= 10
	}
	return arr
}

func calSquares(number int) []int {
	var arr = []int{}
	for number != 0 {
		digit := number % 10
		arr = append(arr, digit * digit)
		number /= 10
	}
	return arr
}

func calSquaresSum(number int) int {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit
		number /= 10
	}
	return sum
}

//THis function squares the given elements in a array
//return nil if the array is empty

func arraySquares(arr []int) []int {
	if len(arr) == 0 {
		return nil
	}
	suareArray := make([]int, len(arr)) //this array is very important, dont try to update the old array if not, when this function is called, the array former value will be updated, which is not what we want
	for i := 0; i < len(arr); i++ {
		suareArray[i] = arr[i] * arr[i]
	}
	return suareArray
}

func main() {

	//number := 193
	//square_digit := SquareDigit(number)
	//square_values := calSquares(number)
	//square_sums := calSquaresSum(number)
	//fmt.Printf("These are the square digits %v in the number: %d: \n",square_digit, number)
	//fmt.Printf("These are the square values %v in the number: %d: \n",square_values, number)
	//fmt.Printf("These are the square sums %v in the number: %d: \n",square_sums, number)

	arr := []int{1,2,3,4, 5}
	ans := arraySquares(arr)
	fmt.Println("this is the original array: ", arr)
	fmt.Printf("These are the squares of the array %v  \n", ans)
}
