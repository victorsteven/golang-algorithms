package stack

type StringStack struct {
	Size int
	Data []string
}

func NewStringStack(len int) *StringStack {
	return &StringStack{
		Size: 0,
		Data: make([]string, len),
	}
}

func (s *StringStack) Push(item string) {
	if s.Size < len(s.Data) {
		s.Data[s.Size] = item
		s.Size++
	}
}

func (s *StringStack) Pop() string {
	if s.Size > 0 {
		item := s.Data[s.Size-1]
		s.Size -= 1

		return item
	} else {
		return ""
	}
}

func (s *StringStack) Front() string {
	if s.Size > 0 {
		return s.Data[s.Size-1]
	} else {
		return ""
	}
}
