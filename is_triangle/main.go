package main

import "fmt"

func main() {
	fmt.Println(isTriangle(5,4,6))
}
func isTriangle(a,b,c int) bool {
	if a+b > c && a+c > b && b+c > a {
		return true;
	}
	return false;
}
