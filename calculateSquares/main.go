package main

import "fmt"

func main() {
	fmt.Println(calSquares(429))
}

func calSquares(number int) int {
	sum := 0
	for number != 0 {
		digit := number % 10 //this means, save the remainder into the the "digits" variable
		fmt.Println("this is a sample digit: ", digit)
		sum += digit * digit
		number /= 10
	}
	return  sum
}