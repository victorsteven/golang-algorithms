package main

func SortArray(arr []int) []int {
	var arrLength = len(arr)
	for i := arrLength - 1;  i >= 0; i-- {
		for j := 1; j <= i; j++ {
			if arr[j-1] > arr[j] {
				var temp = arr[j - 1];
				arr[j-1] = arr[j];
				arr[j] = temp
			}
		}
	}
	return arr
}
