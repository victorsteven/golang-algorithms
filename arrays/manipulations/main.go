package main

import "fmt"



func main(){

	fmt.Println(SortArray([]int{7,5,2,4,3,9})); //[2, 3, 4, 5, 7, 9]
	fmt.Println(SortArray([]int{9,7,5,4,3,1})); //[1, 3, 4, 5, 7, 9]
	fmt.Println(SortArray([]int{1,2,3,4,5,6})); //[1, 2, 3, 4, 5, 6]
}
