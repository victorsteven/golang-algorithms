package main

import "fmt"

func addUniqueElement(arr []int, i int) []int {
	for _, ele := range arr {
		if ele == i {
			return arr
		}
	}
	return append(arr, i)
}
func unique(arr []int) []int {
	keys := make(map[int]bool)
	list := []int{}
	for _, entry := range arr {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return sortArray(list)
}

func sortArray(arr []int) []int {
	arrLen := len(arr)
	for i := arrLen - 1; i >= 0; i-- {
		for j := 1; j <= i; j++ {
			if arr[j-1] > arr[j] {
				var temp = arr[j-1]
				arr[j-1] = arr[j]
				arr[j] = temp
			}
		}
	}
	return arr
}

func main(){
	ans := addUniqueElement([]int{1,4,6,3,7,6,4,9}, 94)
	fmt.Println(ans)

	ansUnique := unique([]int{1,4,6,3,7,6,4,9})
	fmt.Println(ansUnique)
}
