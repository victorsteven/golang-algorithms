package main

import "fmt"

func main() {
	a := []int{1,2,3,4,5,6}
	fmt.Println(theArray(a))
	fmt.Println(theArray2(a))
}

//Since we’re not modifying the array in-place, we can also simplify our algorithm buy doing a more straight-forward element copy:
func theArray(arr []int) []int {
	newArray := make([]int, 0,  len(arr))
	for i := len(arr) - 1; i >= 0; i-- {
		newArray = append(newArray, arr[i])
	}
	return newArray
}
//Simply loop through the first half of the array, swapping each element in turn with its mirror counterpart:
func theArray2(arr []int) []int {
	for i := 0; i < len(arr)/2; i++ {
		j := len(arr) - i - 1
		arr[i], arr[j] = arr[j], arr[i]
	}
	return arr
}
