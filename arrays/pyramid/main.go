package main

import (
	"fmt"
)

func StarsTriangleForward(n int){
	//handle the rows
	for i := 0; i < n;  i++ {
		//handle the columns
		for j := 0; j <= i; j++ {
			fmt.Print("* ")
		}
		fmt.Println()
	}
}
//func StarsTriangleBackward(n int) {
//	for i := 1; i <= n; i++ {
//		for j := 1; j <= n; j++ {
//			if j <= (n-i) {
//				fmt.Print(" ")
//			} else {
//				fmt.Println("* ")
//			}
//		}
//	}
//}

func Triangle(n int) {
	//number of spaces
	k := 2 * n - 2
	//outer loop to handle number of rows, n in this case
	for i := 0; i < n; i++ {
		// inner loop to handle number of spaces
		for j := 0; j < k; j++ {
			fmt.Print(" ")
		}
	//	decrement k after each loop
		k = k - 1
		//inner loop to handle number of columns
		for j := 0; j <= i; j++ {
			fmt.Print("* ")
		}
		//end line after each row
		fmt.Println()
	}
}

//Number patterns
func NumPat(n int) {
//	Intializing the starting number
	num := 1
	//	outer loop to handle number of rows, n in this case
	for i := 0; i < n; i++ {
		//	inner loop to handle number of columns
		for j := 0; j <= i; j++ {
			fmt.Printf("%d ", num)
		}
		num = num + 1

		fmt.Println()
	}
}

func NumNotAssign(n int) {
	num := 1
	for i := 0; i < n; i++ {
		for j := 0; j <= i; j++ {
			fmt.Printf("%d ", num)
			num++
		}
		fmt.Println()
	}
}

//Character Pattern
func alphabet(n int) {
//	initialize the value corresponding to 'A', ASCII value
	num := 65
	for i := 0; i < n; i++ {
		for j := 0; j <= i; j++ {
			ch := string(num)
			fmt.Printf("%v ", ch)
		}
		num++

		fmt.Println()
	}
}
//Continuous character pattern:
func alphabetCont(n int) {
	num := 65
	for i := 0; i < n; i++ {
		for j := 0; j <= i; j++ {
			ch := string(num)
			fmt.Printf("%v ", ch)
			num++
		}
		fmt.Println()
	}
}


func main(){

	alphabetCont(5)

	alphabet(5)

	NumPat(5)
	NumNotAssign(5)

	//Triangle(5)
	//StarsTriangleForward(5)

	//StarsTriangleBackward(5)
}