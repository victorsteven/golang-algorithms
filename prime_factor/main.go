package main

import "fmt"

func main(){
	fmt.Println("this is a sample console")

	result := primeFactor(210)
	fmt.Println("these are the prime numbers: ", result)
}

func primeFactor(number int) []int {
	fmt.Println("We called the prime number function")
	var value []int
	divisor := 2
	for number > 1 {
		if number % divisor == 0 {
			value = append(value, divisor)
			number = number / divisor
		}  else {
			divisor++
		}
	}
	return value
}