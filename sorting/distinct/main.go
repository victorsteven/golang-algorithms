package main

import (
	"fmt"
	"sort"
)

/*
Write a function

func Solution(A []int) int
that, given a zero-indexed array A consisting of N integers, returns the number of distinct values in array A.

Assume that:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].
For example, given array A consisting of six elements such that:

 A[0] = 2    A[1] = 1    A[2] = 1
 A[3] = 2    A[4] = 3    A[5] = 1
the function should return 3, because there are 3 distinct values appearing in array A, namely 1, 2 and 3.

Complexity:

expected worst-case time complexity is O(N*log(N));
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
*/

func main() {
	A := []int{1, 2, 3, 5, 6}
	// A := []int{3, 2, 3, 5, 3}
	// A := []int{1, 1, 1, 1, 1}
	// A := []int{}
	ans := Distinct(A)
	fmt.Println("this is the distinct ans: ", ans)

}

// Sort the array and check every next element. If its different then the next element is unique
func Distinct(A []int) int {

	// Get the length of the array:
	arrLength := len(A)
	// Sort the array:
	sort.Ints(A)
	// set the inital result to 1:
	result := 1

	// if the arrat is empty, result is empty:
	if arrLength == 0 {
		return 0
	}
	//Remember, we start from the second element of the array, because we assume that there is already a first element and we have registered that we have one distinct already: (result = 0)
	//Now for the second iteration, check if it distinct from the first one:
	for i := 1; i < arrLength; i++ {
		if A[i] != A[i-1] {
			result++
		}
	}
	return result
}
