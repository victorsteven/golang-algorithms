package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	//fmt.Println(digital_root(942))
	fmt.Println(splitNum(23245))
	//splitNum()
	fmt.Println(DigitalRoot(23245))
}

//func splitString(str string) []string {
//	//numArray := string(num)
//	s := strings.Split(str, "")
//
//	return s
//}

func splitNum(num int) int {
	stringSlice := strings.Split(strconv.Itoa(num), "")
	if len(stringSlice) == 1 {
		return num //if the number just have digit, return the number
	}
	for len(stringSlice) > 1 {
		sum := 0
		for _, v := range stringSlice {
			elem, _ := strconv.Atoi(v)
			sum += elem
		}
		stringSlice = strings.Split(strconv.Itoa(sum), "")
	}
	ans, _ := strconv.Atoi(stringSlice[0])

	return ans
}

//Alternative
func getSum(n int) int {
	if n > 0 && n < 10 {
		return n
	} else if n <= 0 {
		return 0
	}
	return getSum(n/10) + getSum(n%10)
}

func DigitalRoot(n int) int {
	if n >= 0 && n < 10 {
		return n
	}
	return DigitalRoot(getSum(n))
}


