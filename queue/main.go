package main

import "fmt"

type Queue struct {
	mySlice []int
}
//Adds integer provided to the back of the Queue
func (q *Queue) Enqueue(i int) {
	q.mySlice = append(q.mySlice, i)
}
//Returns the first item in the queue, and removes that item in the Queue
// but panics is there is none
func (q *Queue) Dequeue() int {
	//Return the first item in the queue
	ret := q.mySlice[0]
	//Remove the first item from the queue
	//this is the output from the range below: first element and the last element(note that the last element is: len(q.mySlice)-1)
	q.mySlice = q.mySlice[1:len(q.mySlice)]

	return ret
}
//this will give us a string print of our queue
func (q *Queue) String() string {
	return fmt.Sprint(q.mySlice)
}

func main() {
	var q = new(Queue)
	q.Enqueue(123)
	q.Enqueue(789)
	q.Enqueue(433)
	//q.Enqueue(903)
	//q.Enqueue(783)
	//q.Enqueue(600)
	fmt.Println(q)
	fmt.Println(q.Dequeue())
	fmt.Println(q.Dequeue())
}
