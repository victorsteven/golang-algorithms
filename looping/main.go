package main

import "fmt"

func main() {
	arr := []int{1,2,3,4,5,6,7}
	get_all(arr)
	fmt.Println()
	remove_last(arr)
	fmt.Println()
	still_print_last(arr)
}

//everythin will be printed in this array
func get_all(arr []int) {
	for i := 0; i < len(arr); i++ {
		fmt.Println(arr[i])
	}
}

//the last element will not be printed in this array
func remove_last(arr []int) {
	for i := 0; i < len(arr) - 1; i++ {
		fmt.Println(arr[i])
	}
}

//this will still print that last element
func still_print_last(arr []int) {
	for i := 0; i <= len(arr) - 1; i++ {
		fmt.Println(arr[i])
	}
}
